# PGlift instance
Ansible role to install PostgreSQL instance with Pglift.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name             | Variable description                 | Type    | Default | Required
---                       | ---                                  | ---     | ---     | ---
pgsql_instance_name       | Postgresql instance name             | string  | Yes     | Yes
pgsql_instance_port       | Postgresql instance port             | integer | No      | Yes
pgsql_instance_ssl        | Postgresql instance ssl              | boolean | Yes      | Yes
pgsql_instance_cnx        | Postgresql instance connections      | integer | Yes     | No
pgsql_instance_shbuffers  | Postgresql instance shared buffers   | string  | Yes     | Yes
pgsql_instance_sockdir    | Postgresql instance socket directory | string  | Yes     | No

## Dependencies

* [PGlift Ansible collection](https://galaxy.ansible.com/dalibo/pglift)

## Playbook Example

```
To become
```

## License

GPL v3

## Links

* [PostgreSQL](https://www.postgresql.org/)
* [PGlift](https://pglift.readthedocs.io/en/latest/)
* [pgBackRest](https://pgbackrest.org/)

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)
